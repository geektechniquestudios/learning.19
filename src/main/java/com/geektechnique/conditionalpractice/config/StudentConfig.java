package com.geektechnique.conditionalpractice.config;

import com.geektechnique.conditionalpractice.condition.VerifyStudentCondition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;



@Configuration
public class StudentConfig {

    @Conditional(VerifyStudentCondition.class)
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

//    @Conditional(SomeClass.class)
//    @Bean
//    public SomeBean beanMethod(){
//
//    }
}

