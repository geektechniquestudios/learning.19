package com.geektechnique.conditionalpractice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConditionalPracticeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConditionalPracticeApplication.class, args);
	}

}

//refer to this stack overflow post for good examples: https://stackoverflow.com/questions/35429168/how-to-conditionally-declare-bean-when-multiple-profiles-are-not-active
